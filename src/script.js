// VARIABLES

var Colors = {
  background : "#ed9757",
  select_circle : "#fa2d00",
  sidebar : "#010101"
}

var rocks = {
  // texture that will be assigned in the preload function
  texture : null,
  // load templates from rocks.js if its not empty otherwise use random polygons as rocks
  templates : ROCKS != "" ? JSON.parse(ROCKS) : {random : {type : "random"}},
  // list for future fixed rocks
  fixed : [],
  // list for future freefalling rocks
  falling : []
}

// rocks.templates.forEach(function(r){ r.mesh.uvs.reverse() ; r.mesh.vertices.reverse()})

// var collisions = []

var synth = {
  buffers : [],
  players : [],
  find_free_player : function(){
    var free = -1
    for(var i = 0; i<this.players.length; i++)
      if(!this.players[i].isPlaying()){
        free = i
        break;
      }
    if(free == -1){
      var l = 0
      var f = -1
      var t = 0
      for(var i = 0; i<this.players.length; i++){
        t = this.players[i].currentTime()
        if(t > 0.1 && t >= l)
          f = i
      }
      free = f
    }
    return free
  },
  play : function(sound_index, vol, pan){
    var free = this.find_free_player()
    console.log(sound_index)
    if(free != -1){
      var free_player = this.players[free]
      free_player.buffer = this.buffers[sound_index - 1]
      free_player.setVolume(vol, 0)
      free_player.pan(pan, 0)
      free_player.play()
    }
  },

  create_players : function(){
    for(var i = 1;i<7; i++)
      this.players.push( loadSound("./media/"+i+"rock.mp3") )
  },

  assign_buffers : function(){
    for(var i = 0;i<6; i++)
      this.buffers.push(this.players[i].buffer)
  }
}

// select second rock from templates
var selected_rock = 1

//var cursor = null

// MATTER.js ALIASES
Matter.use('matter-collision-events');
var  World = Matter.World,
    Body = Matter.Body,
    Bodies = Matter.Bodies;
    Constraint = Matter.Constraint;

// PHYSICS SETUP

// create physics world
var engine = Matter.Engine.create();
Matter.Engine.run(engine);
Matter.Engine.update(engine);
var runner = Matter.Runner.create();
Matter.Runner.run(runner, engine);

// enable drag functionality for the mouse
var mouse = Matter.Mouse.create($("#p5_container").get(0)),
mouseConstraint = Matter.MouseConstraint.create(engine, {
    mouse: mouse,
    constraint: { angularStiffness: 0,}
})
World.add(engine.world, mouseConstraint);

// add side-borders
var left = Bodies.rectangle(50, window.innerHeight / 2, 100, window.innerHeight, { isStatic: true });
var right = Bodies.rectangle(window.innerWidth + 5, window.innerHeight / 2, 10, window.innerHeight, { isStatic: true });
World.add(engine.world, [left, right]);

// set gravity
engine.world.gravity.y = 0.6


// FUNCTIONS

// a helper function for creating 2D vectors with x and y coordinates
function v2(x, y){return {x:x, y:y}}
function clamp(a,b,x) {
  return x < a ? a : (x > b ? b : x)
}
function lerp(a, b, t) {
  return a * (1 - t) + b * t
}
function ilerp(a, b, p) {
  return (clamp(a,b,p) - a) / (b - a)
}

// generates random polygon-mesh (only used if no templates found)
function random_mesh(c){
  var sigmoid = (x) => 1 / (1 + Math.pow(Math.E, -x))
  var verts = []
  var uvs = []
  for(var i = 0; i < c; i++){
    var t = Math.PI * 2 * (i / c)
    var radius = 50
    var max = 20
    var x = Math.sin(t) * (radius + max * random(-1,1))
    var y = radius + Math.cos(t) * (radius + max * random(-1,1))
    verts.push(v2(x, y))
    uvs.push(v2(sigmoid(x), sigmoid(y)))
  }
  return {vertices : verts, uvs : uvs}
}
// creates new physics-body from a given rock template at x, y coordinates
function new_rock(i, rock, x, y, fixed){
  var mesh = rock.type == "random" ? random_mesh(6) : rock.mesh
  var r = Matter.Bodies.fromVertices(x, y, mesh.vertices)
  console.log(r)
  r.uvs = mesh.uvs
  r.sound = i % rocks.templates.length
  r.play_sound = function (vol){
    var pan = -1 + 2 * (1 - ilerp(0, window.innerWidth, this.position.x))
    // console.log(pan)
    // random_index = Math.floor(Math.random() * rocks.templates.length)
    synth.play(this.sound + 1, vol, pan)
  }
  r.wrap = function(){
    if(this.position.y > window.innerHeight)
      Body.setPosition(this, v2(this.position.x,0))
    else if(this.position.y < 0)
      Body.setPosition(this, v2(this.position.x, window.innerHeight))
    // // teleport horizontally as well
    // else if(b.position.x < 0)
    //   Body.setPosition(b, v2(window.innerWidth, b.position.y))
    // else if(b.position.x > window.innerWidth)
    //   Body.setPosition(b, v2(0, b.position.y))
  }
  if(!fixed)
    r.onCollide(function(collision) {
      var v = (Math.abs(r.velocity.x) + Math.abs(r.velocity.y))
      var volume = ilerp(0, 100, v)
      // console.log(collision)

      // play own sound
      this.play_sound(volume)

      // play the other rock's sound (except when its the borders)
      var id = collision.bodyA.id == this.id ? collision.bodyB.id : collision.bodyA.id
      if(id > 3){
        // HACK!!!
        let rock = engine.world.bodies.find((b) => b.id == id)

        if(rock){
          rock.play_sound(volume)
        }else{
          console.log("cant find", id)
        }
        

      } 

      // if(collisions.length > 10){
      //   var ck = Object.keys(collision.contacts)
      //   // collisions.push({pos : collision.contacts.supports[0], time : 1, vel : volume})
      //   collisions.push({pos : collision.contacts[ck[0]].vertex, time : 1, vel : volume})
      //   collisions.shift()
      // }
    })
  console.log(r)
  return r
}

// adds rock body to world, either fixed or freefalling
function add_rock(x, y, fixed){
  var r = new_rock(selected_rock, rocks.templates[selected_rock], x, y, fixed)
  rocks[ fixed ? "fixed" : "falling" ].push(r)
  if(fixed){
    var constraint = Constraint.create({
        pointA: { x: x, y: y},
        bodyB: r,
        length: 0
    })
    World.add(engine.world, [r, constraint]);
  }else{
    World.add(engine.world, r);
  }
  // random start angle
  Body.setAngle(r, Math.random() * Math.PI * 2)
}
// teleports rock to top if it fell off the screen (phew)


// DRAWING FUNCTIONS

var Drawer = {
  // draws rock from physics world
  rock : function (b){
    if(b.uvs != undefined){
      texture(rocks.texture)
      // console.log(b.vertices)
      let vxuvOffset = 0
      beginShape()
      b.vertices.forEach((v, i) => vertex(v.x, v.y, 0, b.uvs[(i + vxuvOffset) % b.uvs.length].x, b.uvs[(i + vxuvOffset) % b.uvs.length].y))
      endShape(CLOSE)
    }
  },

  // draws rock icon from rocks.templates into the rock menu
  rock_icon : function(p, b, selected){
    push()
    translate(p.x,p.y)
    if(selected){
      fill(Colors.select_circle)
      noStroke()
      ellipse(0, 0,90, 90)
    }
    // rotate(new Date().getTime() * 0.0001)
    var s = 42 / b.radius
    texture(rocks.texture)
    beginShape()
    b.mesh.vertices.forEach((v, i) => vertex(v.x * s, v.y * s, 0, b.mesh.uvs[i].x, b.mesh.uvs[i].y))
    endShape(CLOSE)
    pop()
  },

  // draws the rocks menu
  rock_menu : function(templates){
    stroke(0)
    fill(Colors.sidebar)
    rect(0,0,100, height)
    var h = height / templates.length;
    for(var i = 0; i<templates.length; i++){
      this.rock_icon(
        v2(50, i * h + h * 0.5),
        templates[i],
        selected_rock == i
      )
    }
  },

  //draws collisions as shrinking yellow circles
  collision : function(c){
    var s = 20
    fill(222,222,0, Math.floor(c.time * 255))
    ellipse(
      c.pos.x+Math.random() * s * (c.vel + c.time),
      c.pos.y+Math.random() * s * (c.vel + c.time),
      c.time * 20,
      c.time * 20
    )
  }
}


// P5.js FUNCTIONS

function preload(){
  rocks.texture = loadImage("./images/rock.png")
  synth.create_players()
}
function setup(){
  synth.assign_buffers()
  createCanvas(window.innerWidth,window.innerHeight, WEBGL).parent("p5_container")

}
function draw(){
  translate(-width/2,-height/2,0)
  background(Colors.background)

  // tries teleporting all rocks
  rocks.falling.forEach((b) => b.wrap())

  // draws all rocks
  rocks.falling.forEach((b) => Drawer.rock(b))
  rocks.fixed.forEach((b) => Drawer.rock(b))

  // // draws collisions, counts them down
  // collisions.forEach(function(c){
  //   // Drawer.collision(c)
  //   c.time = Math.max(0, c.time - 0.1)
  // })

  // draws rock menu
  Drawer.rock_menu(rocks.templates)
}



// EVENT BINDINGS

// disables browser's right-click menu
$(window).on("contextmenu", (e) => e.preventDefault())

// help
$("#help").mouseenter((e) => $("#help").html("left-click : freefaaall <br>right-click : fixd"))
$("#help").mouseleave((e) => $("#help").html("?"))

// adds new rock or selects rock from menu
$(window).mousedown(
  function(e){
    getAudioContext().resume()
    if(e.pageX > 100){
      if(Matter.Query.point(engine.world.bodies, v2(e.pageX,e.pageY)).length == 0)
        add_rock(e.pageX, e.pageY, e.button == 2)

    }
    else
      selected_rock = Math.floor((e.pageY / window.innerHeight) * rocks.templates.length)
  }
)

// destroys rock if it is dragged over the left menu :/
$(window).mouseup(
  function(e){
    if(e.pageX < 100){
      if(mouseConstraint.body != null && mouseConstraint.body.id > 3) // HACK!!!
        World.remove(engine.world, mouseConstraint.body)
    }
  }
)


/* TODO
  scale rocks
  drop points
  teleport zones
*/
